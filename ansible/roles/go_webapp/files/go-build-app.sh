#!/bin/bash

REPO=$1
OUTPUT=$2
GO=/usr/lib/go-1.8/bin/go

export GOPATH=/tmp

if [ ! -d "/tmp/src" ]; then
	mkdir /tmp/src
fi
if [ ! -h "/tmp/src/$(basename $REPO)" ]; then
	ln -s $REPO /tmp/src
fi
cd /tmp/src/$(basename $REPO)
$GO get -v ./...
$GO build -o $OUTPUT