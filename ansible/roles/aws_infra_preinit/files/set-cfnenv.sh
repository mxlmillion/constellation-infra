#!/bin/bash -xe

SET=$1
SECGROUP=$2

echo setting /etc/cfnenv for $SET

if [ "$SET" == "prv_ipv4" ]; then
	IPS=""
	while true; do
		if [ -z "$IPS" ]; then
			sleep 5s
		else
			break
		fi

		for ip in $(/usr/bin/aws ec2 describe-instances --filter "Name=instance.group-id,Values=${SECGROUP}" "Name=instance-state-name,Values=running" | jq -cr '.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress'); do
			IPS=${IPS},$ip
		done
	done
	echo "PRV_IPV4=${IPS#,}" >> /etc/cfnenv
fi

if [ "$SET" == "redis_ips" ]; then
	IPS=""
	while true; do
		if [ -z "$IPS" ]; then
			sleep 5s
		else
			break
		fi

		for ip in $(/usr/bin/aws ec2 describe-instances --filter "Name=instance.group-id,Values=${SECGROUP}" "Name=instance-state-name,Values=running" | jq -cr '.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress'); do
			IPS=${IPS},$ip
		done

		
	done
	echo "REDIS_IPS=${IPS#,}" >> /etc/cfnenv
fi
