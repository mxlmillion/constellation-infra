package main

import (
	"fmt"
	"os"
	"os/exec"
	//"io/ioutil"
	"strings"
	"encoding/json"
	"bufio"
	"errors"
	"regexp"
)

const (
	ENVFILE string = "/etc/cfnenv"
	CONFDDIR = "/etc/cfn/discovery.d"
	CFNVARSFILE = "/etc/cfn/cfn-vars"
	CFN_STACKNAME_VARNAME = "CFN_STACKNAME"
	CFN_STACKNAME_TAGNAME = "CfnStack"
)

// type prv_ipv4 struct {
// 	cmd []string
// 	args [][]string
// }
// func NewPrvIpv4() prv_ipv4 {
// 	return prv_ipv4{

// 	}
// }
func main() {
	// confPath := flag.String("conf", `/etc/cfn/aries-discovery.conf`, "path to configuration file")
	// flag.Parse()

	// Make any env vars set in /etc/cfn/cfn-vars avaiable in the environment
	setEnv()

	dir, _ := os.Open(CONFDDIR)
	defer dir.Close()

	files, _ := dir.Readdir(-1)
	var content string
	var err error
	// Now loop through each conf file and append info obtained to variables in /etc/cfnenv
	for _, file := range files {
		fmt.Printf("performing discovery on %s...\n", CONFDDIR + "/" + file.Name())
		conf, _ := ExtractConfiguration(CONFDDIR + "/" + file.Name())
		fmt.Printf("using conf values: %#v\n", conf)

		switch conf.Setvar {
			case "redis_ips":
				content, err = RunCommand(
					[]string{"/usr/bin/aws", "/usr/bin/jq"},
					[][]string{
						{"ec2", "describe-instances", "--filter", "Name=tag:" + conf.Tagname + ",Values='" + conf.Tagvalue + "'", "Name=tag:" + CFN_STACKNAME_TAGNAME + ",Values='" + os.Getenv(CFN_STACKNAME_VARNAME) + "'"},
						{"-cr", ".Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress"},
					},
				)
				if err != nil {
					fmt.Printf("Encountered error when executing command: '%s'\n", err)
				}
				content = "REDIS_IPS=" + content
				fmt.Printf("Set content to: '%s'\n", content)
			case "prv_ipv4":
				content, err = RunCommand(
					[]string{"/usr/bin/aws", "/usr/bin/jq"},
					[][]string{
						{"ec2", "describe-instances", "--filter", "Name=tag:" + conf.Tagname + ",Values='" + conf.Tagvalue + "'", "Name=instance-state-name,Values=running", "Name=tag:" + CFN_STACKNAME_TAGNAME + ",Values='" + os.Getenv(CFN_STACKNAME_VARNAME) + "'"},
						{"-cr", ".Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress"},
					},
				)
				if err != nil {
					fmt.Printf("Encountered error when executing command: '%s'\n", err)
				}
				content = "PRV_IPV4=" + content
				fmt.Printf("Set content to: '%s'\n", content)
		}

		fmt.Printf("Writing '%s'\n", content)
		f, err := os.OpenFile(ENVFILE, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			fmt.Printf("Encountered error while opening file: '%s'\n", err.Error())
		}
		w := bufio.NewWriter(f)
    	size, err := w.WriteString(content + "\n")
    	if err != nil {
    		fmt.Printf("Error encountered while writing %s: %s\n", ENVFILE, err)
    	} else {
    		fmt.Printf("Wrote %n bytes to %s\n", size, ENVFILE)
    	}
    	w.Flush()
    	f.Close()
	}
}

func RunCommand(cmds []string, args [][]string) (string, error) {
	
	fmt.Printf("Executing cmd '%s' with args '%#v'\n", cmds[0], args[0])
	awsout, err := exec.Command(cmds[0], args[0]...).Output()
	fmt.Printf("Received output: '%s'\n", string(awsout))
	if err != nil {
		return "", errors.New("ERROR: command failure: " + err.Error())
	}

	fmt.Printf("Executing cmd '%s' with args '%#v'\n", cmds[1], args[1])
	jq := exec.Command(cmds[1], args[1]...)
	jq.Stdin = strings.NewReader(string(awsout))
	jqout, err := jq.Output()
	fmt.Printf("Received output: '%s'\n", string(jqout))
	if err != nil {
		return "", errors.New("ERROR: command failure: " + err.Error())
	}

	s := strings.TrimRight(string(jqout), "\n")
	sa := strings.Split(s, "\n")
	a := strings.Join(sa, ",")
	return a, nil

}

func setEnv() {
	fmt.Printf("Importing environment variables from %s\n", CFNVARSFILE)
	f, err := os.Open(CFNVARSFILE)
	if err != nil {
		fmt.Printf("Error opening /etc/cfn/cfn-vars: %s\n", err)
	}
	buf := bufio.NewReader(f)

	for {
		_, err := buf.Peek(1);
		if err != nil {
			fmt.Printf("Finished setting vars\n")
			break
		}

		s, _ := buf.ReadString('\n')
		regex, rerr := regexp.Compile("^(?:export)?\\s?([0-9a-zA-Z_-]+)=(.*)")
		if rerr != nil {
			fmt.Printf("err compiling regexp: %s\n", rerr)
		}
		res := regex.FindAllStringSubmatch(s, -1)
		for _,v := range res {
			fmt.Printf("Setting '%s' with value '%s'\n", v[1], v[2])
			os.Setenv(v[1], v[2])
		}
	}
}
// /usr/bin/aws ec2 describe-instances --filter Name=tag:Name,Values='AriesApp-maxlynch-net' | jq -cr '.Reservations[].Instances[].NetworkInterfaces[].PrivateIpAddresses[].PrivateIpAddress'

type Config struct {
	 Setvar string `json:"setvar"`
	 Tagname string `json:"tagname"`
	 Tagvalue string `json:"tagvalue"`
}
func NewConfig() Config {
	return Config{
		Setvar: "",
		Tagname: "",
		Tagvalue: "",
	}
}

func ExtractConfiguration(file string) (Config, error) {
	
	conf := NewConfig()

	f, err := os.Open(file)
	if err != nil {
		fmt.Println("config file not found.")
		fmt.Println("using default values")
		return conf, err
	}
	err = json.NewDecoder(f).Decode(&conf)

	return conf, err
}

// 