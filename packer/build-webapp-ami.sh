#!/bin/bash 

PLAYBOOK_HOME=~/programming/constellation-infra/ansible
PACKER_HOME=~/programming/constellation-infra/packer
DEBUG=""
VARFILE=""

echo -n "Set debug flag? "
read dflag
if [ ! -z $dflag ] && [ "$dflag" != "n" ]; then
	DEBUG="-debug"
fi

echo "Which app? "
c=0; declare -a APPS
for app in `ls vars/`; do
	echo "$((c+1)): ${app/.vars/}"
	APPS[$c]=$app
	((c++))
done
read app
VARFILE="vars/${APPS[$app-1]}"
echo varfile: $VARFILE 

echo -n "AWS Access Key: "
read KEY
if [ -z $KEY ]; then
	echo "value empty."
	exit 1
fi

echo -n "AWS Secret: "
read SECRET
if [ -z $SECRET ]; then
	echo "value empty."
	exit 1
fi

if [ ! -z $VARFILE ] && [ ! -f $VARFILE ]; then
	echo "variable file $VARFILE does not exist."
	exit 1
fi

# Check playbook syntax first
# cd $PLAYBOOK_HOME
# if ! ansible-playbook --syntax-check $PLAYBOOK; then
# 	echo -e "Syntax check failed.\nAborting."
# 	exit 1
# fi

cd $PACKER_HOME
packer build $DEBUG -var-file $VARFILE -var "aws_access_key=$KEY" -var "aws_secret_key=$SECRET" pk-webapp-ami.json
