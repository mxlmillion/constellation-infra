#!/bin/bash

PLAYBOOK_HOME=~/programming/constellation-infra/ansible
PACKER_HOME=~/programming/constellation-infra/packer
DEBUG=""
VARFILE="base-ami-vars.json"

echo -n "Set debug flag? "
read dflag
if [ ! -z $dflag ]; then
	DEBUG="-debug"
fi

if [ ! -z $VARFILE ] && [ ! -f $VARFILE ]; then
	echo "variable file $VARFILE does not exist."
	exit 1
fi

echo -n "AWS Access Key: "
read KEY
if [ -z $KEY ]; then
	echo "value empty."
	exit 1
fi

echo -n "AWS Secret: "
read SECRET
if [ -z $SECRET ]; then
	echo "value empty."
	exit 1
fi

# Check playbook syntax first
#cd $PLAYBOOK_HOME
#if ! ansible-playbook --syntax-check aws-setup-webapp.yml; then
#	echo -e "Syntax check failed.\nAborting."
#	exit 1
#fi

cd $PACKER_HOME
packer build $DEBUG -var-file $VARFILE -var "aws_access_key=$KEY" -var "aws_secret_key=$SECRET" pk-base-ami.json
